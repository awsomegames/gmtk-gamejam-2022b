using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAttack : EffectProperties
{
    [SerializeField] private EffectProperties parentAttack;
    [SerializeField] private CollisionCallback collisionCallback;
    [SerializeField] private AnimationCallback animationCallback;
    public float projectileSpeed;
    public Transform projectile;
    /*public bool destroyOnFirstImpact;
    public bool destroyOnShield;*/
    public int damageValue;
    public bool damageDone = false;
    public float delayBeforeDestroy;
    public bool fireProjectile = false;

    private void Awake()
    {
        collisionCallback.OnTriggerEnterEvent += OnTriggerEnterAction;
        animationCallback.OnAnimationEvent += FireProjectile;
    }

    private void FixedUpdate()
    {
        if(fireProjectile){
            projectile.localPosition += new Vector3(projectileSpeed * Time.fixedDeltaTime, 0, 0);
        }
    }

    public void FireProjectile(string animationMessage){
        fireProjectile = true;
    }

    private void OnTriggerEnterAction(Collider other)
    {
        Debug.Log("Basic attack hit!");

        // Do not collide with their team
        UnitType attackType = ownerUnitProperties.unitType;
        if(attackType == other.GetComponentInParent<UnitProperties>()?.unitType ||
           attackType == other.GetComponentInParent<ShieldProperties>()?.ownerUnitProperties.unitType){
            return;
        }

        if(damageDone) {
            return;
        }

        StartCoroutine(CollisionAction(other));
    }

    private IEnumerator CollisionAction(Collider other) {
        fireProjectile = false;
        Destroy(projectile.gameObject);
        effectResults = EffectResults.NONE;
        CombatManager.Instance.OnAttackReceived(this, ownerUnitProperties, other.gameObject);
        damageDone = true;
        yield return new WaitForSeconds(delayBeforeDestroy);
        Destroy(gameObject);
        yield return null;
    }

    private void OnDestroy()
    {
        collisionCallback.OnTriggerEnterEvent -= OnTriggerEnterAction;
        animationCallback.OnAnimationEvent -= FireProjectile;

        hasFinishedEffect = true;
        if(parentAttack != null) {
            parentAttack.ChildEffectFinished(this);
        }
        else {
            FinishedEffect();
        }
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        CombatManager.Instance.DamageUnit(receiverUnit, damageValue);
        effectResults = EffectResults.DidHitAnOpponentUnit;
    }

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        receiverShield.BreakShield(this);
        effectResults = EffectResults.DidHitAShield;
    }

    public override void ChildEffectFinished(EffectProperties effectProperties)
    {
        throw new System.NotImplementedException();
    }
}