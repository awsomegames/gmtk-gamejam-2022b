using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornersAttack : EffectProperties
{
    [SerializeField] private BasicAttack upRight;
    [SerializeField] private BasicAttack upLeft;
    [SerializeField] private BasicAttack bottomRight;
    [SerializeField] private BasicAttack bottomLeft;

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        throw new System.NotImplementedException();
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        throw new System.NotImplementedException();
    }

    public override void Init(UnitProperties invoker, Effect effect)
    {
        base.Init(invoker, effect);
        Debug.Log("Invoker from Corners: " + invoker.unitType);
        upRight.Init(invoker, effect);
        upLeft.Init(invoker, effect);
        bottomRight.Init(invoker, effect);
        bottomLeft.Init(invoker, effect);

        childrenEffects = new List<EffectProperties>();
        childrenEffects.Add(upRight);
        childrenEffects.Add(upLeft);
        childrenEffects.Add(bottomRight);
        childrenEffects.Add(bottomLeft);
    }

    public override void ChildEffectFinished(EffectProperties effectProperties) {
        if(HaveAllChildrenEffectsFinished()) {
            hasFinishedEffect = true;
            FinishedEffect();
            Destroy(gameObject);
        }
    }
}