using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleArrowAttack : EffectProperties
{
    public BasicAttack arrowUp;
    public BasicAttack arrowUpRight;
    public BasicAttack arrowRight;

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        throw new System.NotImplementedException();
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        throw new System.NotImplementedException();
    }

    public override void Init(UnitProperties invoker, Effect effect)
    {
        base.Init(invoker, effect);

        childrenEffects = new List<EffectProperties>();
        childrenEffects.Add(arrowUp);
        childrenEffects.Add(arrowUpRight);
        childrenEffects.Add(arrowRight);

        foreach(EffectProperties child in childrenEffects) {
            child.Init(invoker, effect);
        }
    }

    public override void ChildEffectFinished(EffectProperties effectProperties) {
        if(HaveAllChildrenEffectsFinished()) {
            hasFinishedEffect = true;
            FinishedEffect();
            Destroy(gameObject);
        }
    }

}