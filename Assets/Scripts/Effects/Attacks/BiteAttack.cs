using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiteAttack : EffectProperties
{
    [SerializeField] private EffectProperties parentAttack;
    public ParticleSystem[] particles;
    public BasicAttack projectile;
    public int healValue;
    public float delayBeforeDestroy;

    public override void Init(UnitProperties invoker, Effect effect) {
        base.Init(invoker, effect);
        projectile.Init(invoker, effect);
    }

    private void OnDestroy()
    {
        hasFinishedEffect = true;
        if(parentAttack != null) {
            parentAttack.ChildEffectFinished(this);
        }
        else {
            FinishedEffect();
        }
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        CombatManager.Instance.HealUnit(ownerUnitProperties, healValue);
    }

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        receiverShield.BreakShield(this);
    }

    public override void ChildEffectFinished(EffectProperties effectProperties)
    {
        StartCoroutine(EffectAction());
    }

    private IEnumerator EffectAction() {
        /*foreach(ParticleSystem p in particles) {
            p.Play();
        }*/
        if(projectile.effectResults == EffectResults.DidHitAnOpponentUnit) {
            ApplyEffectToUnit(null);
            yield return new WaitForSeconds(delayBeforeDestroy);
        }
        hasFinishedEffect = true;
        Destroy(gameObject);
        yield return null;
    }
}
