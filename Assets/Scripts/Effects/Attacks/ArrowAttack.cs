using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAttack : EffectProperties
{
    public BasicAttack baseAttackProjectile;

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        throw new System.NotImplementedException();
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        throw new System.NotImplementedException();
    }

    public override void Init(UnitProperties invoker, Effect effect)
    {
        base.Init(invoker, effect);
        baseAttackProjectile.Init(invoker, effect);

        childrenEffects = new List<EffectProperties>();
        childrenEffects.Add(baseAttackProjectile);
    }

    public override void ChildEffectFinished(EffectProperties effectProperties) {
        if(HaveAllChildrenEffectsFinished()) {
            hasFinishedEffect = true;
            FinishedEffect();
            Destroy(gameObject);
        }
    }

}