using UnityEngine;

[CreateAssetMenu(fileName = "Face", menuName = "Dice/Spawnable Effect", order = 1)]
public class SpawnableEffect : Effect
{
    public GameObject effectPrefab;
    public bool basedOnMovedDirection;
    private GameObject spawnedObject = null;

    public override void Execute(UnitProperties invoker, Vector3 movedDirection, DiceFaceData faceData)
    {
        spawnedObject = Instantiate(effectPrefab, invoker.transform.position, Quaternion.identity);
        if(basedOnMovedDirection) {
            if(movedDirection == Vector3.right) {
                // Default effect direction
            }
            else if(movedDirection ==  Vector3.left) {
                spawnedObject.transform.Rotate(new Vector3(0, 0, 180));
            }
            else if(movedDirection ==  Vector3.up) {
                spawnedObject.transform.Rotate(new Vector3(0, 0, 90));
            }
            else if(movedDirection ==  Vector3.down) {
                spawnedObject.transform.Rotate(new Vector3(0, 0, -90));
            }
        }
        else {
            spawnedObject.transform.Rotate(faceData.transform.eulerAngles);
        }

        spawnedObject.GetComponent<EffectProperties>().Init(invoker, this);
    }
}