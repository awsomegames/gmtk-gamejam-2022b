using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectResults {
    NotExecuted,
    DidHitAnOpponentUnit,
    DidHitAShield,
    NONE
}

public abstract class EffectProperties : MonoBehaviour
{
    [HideInInspector] public UnitProperties ownerUnitProperties;
    //public event Action OnEffectFinished;
    [HideInInspector] public Effect effect;
    [HideInInspector] public bool hasFinishedEffect = false;
    [HideInInspector] public List<EffectProperties> childrenEffects;
    public EffectResults effectResults = EffectResults.NotExecuted;

    public virtual void Init(UnitProperties invoker, Effect effect)
    {
        Debug.Log("invoker: " + invoker.unitType);
        this.ownerUnitProperties = invoker;
        this.effect = effect;
    }

    public void FinishedEffect(){
        effect?.FinishEffect(ownerUnitProperties, effect, this);
    }
    public abstract void ChildEffectFinished(EffectProperties effectProperties);
    public bool HaveAllChildrenEffectsFinished() {
        foreach(EffectProperties child in childrenEffects) {
            if(!child.hasFinishedEffect) {
                return false;
            }
        }
        return true;
    }

    public abstract void ApplyEffectToUnit(UnitProperties receiverUnit);
    public abstract void ApplyEffectToShield(ShieldProperties receiverShield);
}