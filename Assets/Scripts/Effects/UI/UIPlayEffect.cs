using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UIPlayEffect", menuName = "Dice/UIEffects/UIPlayerEffect", order = 1)]
public class UIPlayEffect : Effect
{
    public override void Execute(UnitProperties invoker, Vector3 movedDirection, DiceFaceData faceData)
    {
        TurnManager.Instance.SwitchTurnTo(TurnType.None);
        SceneManagerScript.Instance.LoadLevelByName("Level_01", 0.5f);
    }

}
