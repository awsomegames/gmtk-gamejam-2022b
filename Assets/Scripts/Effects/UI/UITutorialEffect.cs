using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UITutorialEffect", menuName = "Dice/UIEffects/UITutorialEffect", order = 1)]
public class UITutorialEffect : Effect
{
    public override void Execute(UnitProperties invoker, Vector3 movedDirection, DiceFaceData faceData)
    {
        TurnManager.Instance.SwitchTurnTo(TurnType.None);
        SceneManagerScript.Instance.LoadLevelByName("Tutorial", 0.5f);
    }

}
