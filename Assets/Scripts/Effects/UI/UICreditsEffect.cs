using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UICreditsEffect", menuName = "Dice/UIEffects/UICreditsEffect", order = 1)]
public class UICreditsEffect : Effect
{
    public override void Execute(UnitProperties invoker, Vector3 movedDirection, DiceFaceData faceData)
    {
        TurnManager.Instance.SwitchTurnTo(TurnType.None);
        SceneManagerScript.Instance.LoadLevelByName("Credits", 0.5f);
    }

}
