using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UIQuitEffect", menuName = "Dice/UIEffects/UIQuitEffect", order = 1)]
public class UIQuitEffect : Effect
{
    public override void Execute(UnitProperties invoker, Vector3 movedDirection, DiceFaceData faceData)
    {
        TurnManager.Instance.SwitchTurnTo(TurnType.None);
        Application.Quit();
    }

}
