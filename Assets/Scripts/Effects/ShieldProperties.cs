using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShieldProperties : EffectProperties
{
    public virtual void DeactivateShield() {
        Destroy(gameObject);
    }

    public virtual void BreakShield(EffectProperties attackEffect) {
        // TODO: Play break animation
        Destroy(gameObject);
    }
}