using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DualShield : ShieldProperties
{
    [SerializeField] private Shield shieldRight;
    [SerializeField] private Shield shieldLeft;

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        throw new System.NotImplementedException();
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        throw new System.NotImplementedException();
    }

    public override void ChildEffectFinished(EffectProperties effectProperties)
    {
        throw new System.NotImplementedException();
    }

    public override void Init(UnitProperties invoker, Effect effect)
    {
        base.Init(invoker, effect);
        shieldRight.Init(ownerUnitProperties, effect);
        shieldLeft.Init(ownerUnitProperties, effect);
    }
}