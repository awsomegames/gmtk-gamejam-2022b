using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : ShieldProperties
{
    private void Start()
    {
        try {
            TurnManager.Instance.OnTurnSwitched += TryDeactivateShield;
            ownerUnitProperties.OnDeath += DeactivateShield;
            FinishedEffect();
        }
        catch(System.NullReferenceException ex) {
            Debug.Log("ERROR: " + ex.Message);
        }
    }

    public void TryDeactivateShield(TurnType newTurn) {
        if(newTurn == TurnType.Player && ownerUnitProperties.unitType == UnitType.ALLY) {
            DeactivateShield();
            return;
        }

        if(newTurn == TurnType.Enemy && ownerUnitProperties.unitType == UnitType.ENEMY) {
            DeactivateShield();
            return;
        }
    }

    public override void DeactivateShield() {
        Destroy(gameObject);
    }

    public void DeactivateShield(UnitProperties unitProperties) {
        Destroy(gameObject);
    }

    public override void BreakShield(EffectProperties attackEffect) {
        // TODO: Play break animation
        Debug.Log("Shield broke!");
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (ownerUnitProperties != null)
        {
            TurnManager.Instance.OnTurnSwitched -= TryDeactivateShield;
            ownerUnitProperties.OnDeath -= DeactivateShield;
        }
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        throw new System.NotImplementedException();
    }

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        throw new System.NotImplementedException();
    }

    public override void ChildEffectFinished(EffectProperties effectProperties)
    {
        throw new System.NotImplementedException();
    }
}