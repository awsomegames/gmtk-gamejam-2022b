using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType {
    Attack,
    Shield,
    Heal,
    Other
}

public abstract class Effect : ScriptableObject {
    public string effectName;
    public EffectType effectType;
    public event Action<UnitProperties, Effect, EffectProperties> OnEffectFinished;

    public abstract void Execute(UnitProperties invoker, Vector3 movedDirection, DiceFaceData faceData);
    public void FinishEffect(UnitProperties invoker, Effect effect, EffectProperties effectProperties) {
        OnEffectFinished?.Invoke(invoker, effect, effectProperties);
    }
}