using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : EffectProperties
{
    [SerializeField] private EffectProperties parentAttack;
    [SerializeField] private AnimationCallback animationCallback;
    public float delayBeforeDestroy;
    public int healValue;

    private void Start()
    {
        StartCoroutine(DestroyCoroutine());
    }

    private IEnumerator DestroyCoroutine(){
        yield return new WaitUntil(() => ownerUnitProperties != null);
        ApplyEffectToUnit(ownerUnitProperties);
        yield return new WaitForSeconds(delayBeforeDestroy);
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        hasFinishedEffect = true;
        if(parentAttack != null) {
            parentAttack.ChildEffectFinished(this);
        }
        else {
            FinishedEffect();
        }
    }

    public override void ApplyEffectToUnit(UnitProperties receiverUnit)
    {
        CombatManager.Instance.HealUnit(receiverUnit, healValue);
    }

    public override void ApplyEffectToShield(ShieldProperties receiverShield)
    {
        receiverShield.BreakShield(this);
    }

    public override void ChildEffectFinished(EffectProperties effectProperties)
    {
        throw new System.NotImplementedException();
    }
}