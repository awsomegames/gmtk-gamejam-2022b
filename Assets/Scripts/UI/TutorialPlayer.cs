using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;

public class TutorialPlayer : MonoBehaviour
{
    public VideoPlayer[] videos;
    [SerializeField] private Button nextButton;
    [SerializeField] private Button prevButton;
    private int videoIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        videos[videoIndex].Play();
    }

    public void NextVideo() {
        videos[videoIndex].Stop();
        videos[videoIndex].gameObject.SetActive(false);
        videoIndex++;
        PlayVideo(videos[videoIndex]);

        if(videoIndex == videos.Length - 1) {
            nextButton.gameObject.SetActive(false);
        }
        else {
            nextButton.gameObject.SetActive(true);
        }

        if(videoIndex == 0) {
            prevButton.gameObject.SetActive(false);
        }
        else {
            prevButton.gameObject.SetActive(true);
        }
    }

    public void PrevVideo() {
        videos[videoIndex].Stop();
        videos[videoIndex].gameObject.SetActive(false);
        videoIndex--;
        PlayVideo(videos[videoIndex]);

        if(videoIndex == videos.Length) {
            nextButton.gameObject.SetActive(false);
        }
        else {
            nextButton.gameObject.SetActive(true);
        }

        if(videoIndex == 0) {
            prevButton.gameObject.SetActive(false);
        }
        else {
            prevButton.gameObject.SetActive(true);
        }
    }

    public void PlayVideo(VideoPlayer video) {
        video.gameObject.SetActive(true);
        video.Play();
    }
}
