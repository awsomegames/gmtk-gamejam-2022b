using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    [SerializeField] private Player player;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private UnitInfoPanel unitInfoPanel;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private TurnPanel turnPanel;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        player.OnUnitSelection += ShowUnitInfo;
        player.OnUnitDeselection += HideUnitInfo;

        TurnManager.Instance.OnTurnSwitched += ShowTurn;
    }

    public void ShowUnitInfo(UnitProperties unit) {
        Debug.Log("Showing unit info");
        // TODO: Calculate if show up above or bottom
        if(unit.transform.position.y >= mainCamera.transform.position.y + 2 &&
           unit.transform.position.x >= mainCamera.transform.position.x)
        {
            unitInfoPanel.PositionAtBottom();
        }
        else {
            unitInfoPanel.PositionAtUp();
        }
        unitInfoPanel.SetUpInfo(unit);
        // TODO: Show up animation
        unitInfoPanel.ShowUp();
    }

    public void HideUnitInfo(UnitProperties unit) {
        // TODO: Hide animation
        unitInfoPanel.Hide();
    }

    public void ShowPauseMenu() {
        if(pauseMenu == null) {
            return;
        }
        pauseMenu?.SetActive(true);
    }

    public void HidePauseMenu() {
        if(pauseMenu == null) {
            return;
        }
        pauseMenu?.SetActive(false);
    }

    public void ShowTurn(TurnType turnType) {
        turnPanel.SetUp(turnType);
        turnPanel.ShowTurnAnimation();
    }

    public void ShowWinner(string text, UnitType unitType){
        turnPanel.SetUp(text, unitType);
        turnPanel.ShowTurnAnimation(0.5f);
    }
}
