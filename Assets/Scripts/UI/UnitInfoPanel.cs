using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UnitInfoPanel : MonoBehaviour
{
    [SerializeField] private GameObject healthBarPrefab;
    [SerializeField] private Transform parentForHealthBars;
    [SerializeField] private float healtBarSpacing;
    private Animator animator;
    private RectTransform rectTransform;
    [SerializeField] private float upVerticalPosition;
    [SerializeField] private float bottomVerticalPosition;
    [SerializeField] private TMP_Text unitTypeText;
    [SerializeField] private Color allyTextColor;
    [SerializeField] private Color enemyTextColor;
    private Dictionary<UnitType, Color> colorByUnitType;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        rectTransform = GetComponent<RectTransform>();

        colorByUnitType = new Dictionary<UnitType, Color>();
        colorByUnitType.Add(UnitType.ALLY, allyTextColor);
        colorByUnitType.Add(UnitType.ENEMY, enemyTextColor);
    }

    public void ShowUp() {
        animator.Play("ShowUp", -1, 0f);
    }

    public void Hide() {
        animator.Play("Hide", -1, 0f);
    }

    public void PositionAtUp(){
        rectTransform.localPosition = new Vector3(
            rectTransform.localPosition.x,
            upVerticalPosition,
            rectTransform.localPosition.z
        );
    }

    public void PositionAtBottom(){
        rectTransform.localPosition = new Vector3(
            rectTransform.localPosition.x,
            bottomVerticalPosition,
            rectTransform.localPosition.z
        );
    }

    private List<RectTransform> currentHealthBars = null;

    public void SetUpInfo(UnitProperties unit) {
        // TODO: Update unit type text
        unitTypeText.text = unit.unitType.ToString();
        unitTypeText.color = colorByUnitType[unit.unitType];

        ClearHealthBars();
        currentHealthBars = new List<RectTransform>();
        // TODO: Update health bar
        for (int i = 0; i < unit.currentHealth; i++)
        {
            GameObject obj = Instantiate(healthBarPrefab);
            obj.transform.SetParent(parentForHealthBars);
            RectTransform rect = obj.GetComponent<RectTransform>();
            rect.localPosition = new Vector3(
                rect.position.x + i * healtBarSpacing,
                rect.position.y,
                rect.position.z
            );
            rect.localScale = Vector3.one;
            currentHealthBars.Add(rect);
        }
    }

    private void ClearHealthBars() {
        if (currentHealthBars == null) { return; }
        foreach(RectTransform rect in currentHealthBars) {
            Destroy(rect.gameObject);
        }
        currentHealthBars.Clear();
    }

}
