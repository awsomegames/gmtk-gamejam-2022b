using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TurnPanel : MonoBehaviour
{

    [SerializeField] private TMP_Text text;
    [SerializeField] private Image image;
    [SerializeField] private Color playerColor;
    [SerializeField] private Color enemyColor;
    [SerializeField] private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void SetUp(TurnType turnType) {
        if(turnType == TurnType.Player) {
            text.text = "PLAYER TURN";
            image.color = playerColor;
        }
        else if(turnType == TurnType.Enemy) {
            text.text = "ENEMY TURN";
            image.color = enemyColor;
        }
    }

    public void SetUp(string newText, UnitType unitType) {
        text.text = newText;
        if(unitType == UnitType.ALLY) {
            image.color = playerColor;
        }
        else if(unitType == UnitType.ENEMY) {
            image.color = enemyColor;
        }
    }

    public void ShowTurnAnimation() {
        text.gameObject.SetActive(true);
        image.gameObject.SetActive(true);
        animator.speed = 1;
        animator.Play("Show", -1, 0f);
    }

    public void ShowTurnAnimation(float speedMultiplier) {
        text.gameObject.SetActive(true);
        image.gameObject.SetActive(true);
        animator.speed = speedMultiplier;
        animator.Play("Show", -1, 0f);
    }

    public void Hide() {
        text.gameObject.SetActive(false);
        image.gameObject.SetActive(false);
    }

    public void BeginTurn() {
        Debug.Log("End showing turn from Turn Panel!");
        TurnManager.Instance.EndShowingTurn();
    }
}
