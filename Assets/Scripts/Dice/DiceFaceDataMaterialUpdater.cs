using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(DiceFaceData))]
public class DiceFaceDataMaterialUpdater : MonoBehaviour{
    private DiceFaceData diceFaceData;
    private MeshRenderer meshRenderer;
    private UnitProperties unitProperties;
    public bool deactivateUpdate = false;

    void Update()
    {
        if (deactivateUpdate) { return; }
        meshRenderer = GetComponent<MeshRenderer>();
        diceFaceData = GetComponent<DiceFaceData>();
        unitProperties = GetComponentInParent<UnitProperties>();
        if(diceFaceData != null){
            if(diceFaceData.faceProperties != null) {
                Material newMaterialPlayer = diceFaceData.faceProperties.material;
                Material newMaterialEnemy = diceFaceData.faceProperties.enemyMaterial;
                if(unitProperties.unitType == UnitType.ALLY) {
                    AutoChangeMaterial(newMaterialPlayer);
                }
                else if(unitProperties.unitType == UnitType.ENEMY){
                    if(newMaterialEnemy == null) {
                        throw new System.Exception(
                            "There is no Enemy Material assigned to this Face Properties! " +
                            transform.name
                        );
                    }
                    AutoChangeMaterial(newMaterialEnemy);
                }
            }
        }
    }

    private void AutoChangeMaterial(Material newMaterial)
    {
        if (meshRenderer.sharedMaterials.Length > 0 &&
            newMaterial != meshRenderer.sharedMaterials[0])
        {
            meshRenderer.sharedMaterials = new Material[] { newMaterial };
            Debug.Log("Material updated for " + transform.name + " to " + newMaterial.name);
        }
    }
}