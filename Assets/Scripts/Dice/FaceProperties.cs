using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Face", menuName = "Dice/Face Properties", order = 1)]
public class FaceProperties : ScriptableObject
{
    public Material material;
    public Material enemyMaterial;
    public string faceName;
    public Effect effect;

}
