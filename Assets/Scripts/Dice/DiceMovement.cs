using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceMovement : MonoBehaviour
{
    public FaceDetector faceDetector;
    [SerializeField] private Transform parent;
    public UnitProperties unitProperties;
    [SerializeField] private Material alreadyMovedMaterial;
    private DiceFaces diceFaces;
    private bool isMoving = false;

    public event Action<UnitProperties, DiceFaceData, Vector3> FininshedMovement;
    public event Action<UnitProperties, Vector3> StartedMovement;
    public event Action<UnitProperties, Effect> OnEffectExecuted;
    public event Action<UnitProperties, Effect, EffectProperties> OnEffectFinished;

    private void Awake()
    {
        diceFaces = GetComponent<DiceFaces>();
        unitProperties = GetComponentInParent<UnitProperties>();
    }

    public void MoveToDirecion(Vector3 direction, bool executeEffect = true, bool grayOut = true) {
        if (isMoving) { return; }
        if (direction == Vector3.zero) {
            _FininshedMovement(direction,
                executeEffect: false,
                grayOut: true
            );
            DiceFaceData faceData = faceDetector.GetCurrentUpFace();
            FininshedMovement?.Invoke(unitProperties, faceData, direction);
            OnEffectFinished?.Invoke(unitProperties, null,  null);
            return;
        }
        StartCoroutine(MoveToDirectionCoroutine(direction, executeEffect, grayOut));
    }

    private IEnumerator MoveToDirectionCoroutine(Vector3 direction, bool executeEffect, bool grayOut) {
        // TODO: Move 1 unit
        isMoving = true;
        StartedMovement?.Invoke(unitProperties, direction);
        Vector3 currentRotation = transform.eulerAngles;

        float degreeCounter = 0;
        float degreeIntervalValue = 3 * 90/60;
        float delayInterval = 1/60;
        
        // Vector3 originalRotation = Vector3.zero;
        Vector3 originalRotation = transform.eulerAngles;
        Vector3 originalPosition = transform.position;

        float positionIntervalValue = (1f * degreeIntervalValue) / 90f;

        Vector3 degreeInterval = GetRotationAxisForDirection(direction) * degreeIntervalValue;
        Vector3 positionInterval = direction * positionIntervalValue;

        while (degreeCounter < 90){
            //parent.eulerAngles += degreeInterval;
            if(degreeCounter + degreeIntervalValue > 90) {
                degreeIntervalValue = 90 - degreeCounter;
                degreeInterval = degreeInterval.normalized * degreeIntervalValue;
            }
            degreeCounter += degreeIntervalValue;
            parent.Rotate(degreeInterval, Space.Self);
            parent.position += positionInterval;
            yield return new WaitForSeconds(delayInterval);
        }

        parent.position = originalPosition + direction * 1;

        //parent.eulerAngles = originalRotation;
        //parent.Rotate(GetRotationAxisForDirection(direction) * 90f, Space.Self);

        transform.localEulerAngles = transform.eulerAngles;
        parent.eulerAngles = Vector3.zero;

        _FininshedMovement(direction, executeEffect, grayOut);
        yield return null;
    }

    private void _FininshedMovement(Vector3 direction, bool executeEffect = true, bool grayOut = true) {
        isMoving = false;
        unitProperties.hasAlreadyMovedThisTurn = true;

        if(grayOut) {
            DiceFaces.ChangeFacesSecondMaterial(diceFaces, alreadyMovedMaterial);
        }

        if(executeEffect) {
            DiceFaceData faceData = faceDetector.GetCurrentUpFace();
            FaceProperties face = faceData.faceProperties;
            //Debug.Log("Current Face: " + face.faceName);
            //Debug.Log("Current face rotation: " + faceData.transform.eulerAngles);
            FininshedMovement?.Invoke(unitProperties, faceData, direction);


            if(face.effect == null) {
                FinishedEffect(unitProperties, null, null);
            }
            else {
                face.effect.OnEffectFinished += FinishedEffect;
                face.effect?.Execute(unitProperties, direction, faceData);
                OnEffectExecuted?.Invoke(unitProperties, face.effect);
            }
        }
    }

    private void FinishedEffect(UnitProperties unit, Effect effect, EffectProperties effectProperties){
        OnEffectFinished?.Invoke(unit, effect, effectProperties);
    }

    private Vector3 GetRotationAxisForDirection(Vector3 direction) {
        return new Vector3(
            direction.y,
            -direction.x,
            direction.z
        );
    }

    public void RefreshUnit() {
        DiceFaces.RemoveFacesSecondMaterial(diceFaces);
        unitProperties.hasAlreadyMovedThisTurn = false;
    }
}
