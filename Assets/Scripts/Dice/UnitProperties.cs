using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType {
    ALLY,
    ENEMY,
    NONE
}

public class UnitProperties : MonoBehaviour
{
    public UnitType unitType;
    public DiceMovement diceMovement { get; private set; }
    public bool hasAlreadyMovedThisTurn = false;
    public int maxHealth;
    public int currentHealth;
    public ParticleSystem deathParticles;
    public Material playerDeathParticlesMaterial;
    public Material enemyDeathParticlesMaterial;
    public Animator animator;

    public event Action<UnitProperties> OnDeath;

    private void Awake()
    {
        diceMovement = GetComponent<DiceMovement>();
        animator = GetComponentInParent<Animator>();
    }

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void Die() {
        Debug.Log("Death!");

        ParticleSystem particles = Instantiate(deathParticles.gameObject, transform.position, Quaternion.identity).GetComponent<ParticleSystem>();
        var psr = particles.GetComponent<ParticleSystemRenderer>();
        psr.sharedMaterial = playerDeathParticlesMaterial;
        if(unitType == UnitType.ENEMY) {
            psr.sharedMaterial = enemyDeathParticlesMaterial;
        }
        particles.Play();

        // TODO: Unsusbribe from all events in dice all scripts
        OnDeath?.Invoke(this);
        // TODO: Death animation
        Destroy(transform.parent.gameObject);
    }

}