using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceDetector : MonoBehaviour
{
    public DiceFaceData GetCurrentUpFace() {
        RaycastHit hit;
        bool didHit = Physics.Raycast(transform.position, Vector3.forward, out hit, 1f);
        Debug.DrawRay(transform.position, Vector3.forward * 1, Color.yellow, 1f);
        if(didHit) {
            return hit.collider.GetComponent<DiceFaceData>();
        }
        throw new System.Exception("No face detected!");
    }
}
