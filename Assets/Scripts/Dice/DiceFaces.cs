using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceFaces : MonoBehaviour
{
    public DiceFaceData faceAbove;
    public DiceFaceData faceBelow;
    public DiceFaceData faceRight;
    public DiceFaceData faceLeft;
    public DiceFaceData faceFront;
    public DiceFaceData faceBack;
    public List<DiceFaceData> faces = null;
    
    private void Start()
    {
        faces = new List<DiceFaceData>();
        faces.Add(faceAbove);
        faces.Add(faceBelow);
        faces.Add(faceRight);
        faces.Add(faceLeft);
        faces.Add(faceFront);
        faces.Add(faceBack);
    }

    public DiceMovement MakeUntangibleClone(Material extraMaterial) {
        GameObject obj = Instantiate(transform.parent.gameObject);
        obj.transform.localScale *= 0.99f;
        DiceFaces objDiceFaces = obj.GetComponentInChildren<DiceFaces>();
        ChangeFacesSecondMaterial(objDiceFaces, extraMaterial);
        foreach(Collider c in obj.GetComponentsInChildren<Collider>()) {
            c.enabled = false;
        }
        DiceMovement objDiceMovement = obj.GetComponentInChildren<DiceMovement>();
        //objDiceMovement.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        return objDiceMovement;
    }

    public static void ChangeFacesSecondMaterial(DiceFaces diceFaces, Material extraMaterial) {
        foreach(DiceFaceData face in diceFaces.faces) {
            MeshRenderer meshRenderer = face.GetComponent<MeshRenderer>();
            meshRenderer.sharedMaterials = new Material[] {
                meshRenderer.sharedMaterials[0],
                extraMaterial
            };
        }
    }

    public static void RemoveFacesSecondMaterial(DiceFaces diceFaces) {
        foreach(DiceFaceData face in diceFaces.faces) {
            MeshRenderer meshRenderer = face.GetComponent<MeshRenderer>();
            meshRenderer.sharedMaterials = new Material[] {
                meshRenderer.sharedMaterials[0]
            };
        }
    }
}
