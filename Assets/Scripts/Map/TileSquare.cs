using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType {
    Floor,
    Wall,
    Selectable
}

public class TileSquare : MonoBehaviour
{
    public TileType tileType;
}
