using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputListener : MonoBehaviour
{
    [SerializeField] private Player player;
    private Camera mainCamera;
    [SerializeField] private ArrowPointer arrowPointerPrefab;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float maxRayDistanceFromCamera;
    private Vector2 mousePosition;

    private void Awake()
    {
        mainCamera = FindObjectOfType<Camera>();
    }

    private void OnPoint(InputValue inputValue) {
        mousePosition = inputValue.Get<Vector2>();
    }

    private void OnLeftClick(InputValue inputValue) {
        float click = inputValue.Get<float>();
        if (TurnManager.Instance.turnType == TurnType.None) { return; }
        if (GameManager.Instance.isGamePaused) { return; }
        if(TurnManager.Instance.turnType == TurnType.Player &&
           TurnManager.Instance.turnPhase == TurnPhase.Selecting)
        {
            CheckPlayerTurnClicks(click);
        }
    }

    private void CheckPlayerTurnClicks(float click) {
        if(click == 1) {
            Ray ray = Camera.main.ScreenPointToRay(mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, maxRayDistanceFromCamera, layerMask)) {
                //Debug.Log(hit.collider.name);

                DiceMovement diceMovement = hit.collider.GetComponent<DiceMovement>();
                if(diceMovement != null) {
                    player.SelectUnit(diceMovement);
                    return;
                }
                // TODO: Floor tile
                TileSquare tile = hit.collider.GetComponent<TileSquare>();
                if(tile != null) {
                    player.SelectTile(tile);
                    return;
                }

                player.DeselectCurrentUnit();
            }
        }
    }

    public void OnPause() {
        Debug.Log("Pausing");
        GameManager.Instance.TogglePause();
    }

    public void OnArrowPoint() {
        /*Vector3 worldPosition = mainCamera.ScreenToWorldPoint(mousePosition);
        worldPosition = new Vector3(
            worldPosition.x,
            worldPosition.y,
            -1
        );*/
        Ray ray = Camera.main.ScreenPointToRay(mousePosition);
            RaycastHit hit;
        if (Physics.Raycast(ray, out hit, maxRayDistanceFromCamera, layerMask))
        {
            Vector3 pos = new Vector3(
                hit.point.x,
                hit.point.y,
                -1
            );
            ArrowPointer obj = Instantiate(arrowPointerPrefab, pos, Quaternion.identity);
            //obj.transform.SetParent(UIManager.Instance.transform);
        }
    }
}
