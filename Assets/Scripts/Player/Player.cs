using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    DiceMovement selectedUnit = null;
    [SerializeField] private GameObject selectedUnitPrefab;
    [SerializeField] private GameObject selectableTilePrefab;
    [SerializeField] private GameObject selectaedTilePrefab;
    [SerializeField] private Material untangibleCloneMaterial;
    [SerializeField] private Material untangibleCloneMaterialEnemy;
    private List<GameObject> selectableTiles = null;
    private GameObject highlight;

    public event Action<UnitProperties> OnUnitSelection;
    public event Action<UnitProperties> OnUnitDeselection;

    public void SelectUnit(DiceMovement diceMovement) {
        if(selectedUnit == diceMovement) {
            if(selectedUnit.unitProperties.unitType == UnitType.ALLY) {
                selectedUnit.MoveToDirecion(Vector3.zero);
                DeselectCurrentUnit();
            }
            else {
                DeselectCurrentUnit();
            }
            return;

        }
        DeselectCurrentUnit();
        selectedUnit = diceMovement;
        highlight = Instantiate(selectedUnitPrefab, selectedUnit.transform.position + Vector3.forward * 0.49f, Quaternion.identity);
        OnUnitSelection?.Invoke(diceMovement.unitProperties);
        if(!diceMovement.unitProperties.hasAlreadyMovedThisTurn) {
            ShowAvailableMoveSpaces(selectedUnit.unitProperties.unitType);
        }
    }

    public void DeselectCurrentUnit() {
        if(selectedUnit == null) { return; }
        if(highlight != null) {
            Destroy(highlight.gameObject);
        }
        if(selectableTiles != null) {
            foreach(GameObject tile in selectableTiles) {
                Destroy(tile.gameObject);
            }
            selectableTiles.Clear();
        }
        if(selectableMovesDice != null) {
            foreach(DiceMovement dice in selectableMovesDice) {
                Destroy(dice.transform.parent.gameObject);
            }
            selectableMovesDice.Clear();
        }
        OnUnitDeselection?.Invoke(selectedUnit.unitProperties);
        selectedUnit = null;
    }

    private List<DiceMovement> selectableMovesDice = null;
    private void ShowAvailableMoveSpaces(UnitType unitType) {
        if(unitType == UnitType.ALLY) {
            selectableTiles = new List<GameObject>();
            GameObject tile;
            tile = Instantiate(selectableTilePrefab, selectedUnit.transform.position + Vector3.up, Quaternion.identity);
            selectableTiles.Add(tile);
            tile = Instantiate(selectableTilePrefab, selectedUnit.transform.position + Vector3.down, Quaternion.identity);
            selectableTiles.Add(tile);
            tile = Instantiate(selectableTilePrefab, selectedUnit.transform.position + Vector3.left, Quaternion.identity);
            selectableTiles.Add(tile);
            tile = Instantiate(selectableTilePrefab, selectedUnit.transform.position + Vector3.right, Quaternion.identity);
            selectableTiles.Add(tile);
        }

        selectableMovesDice = new List<DiceMovement>();

        Material cloneMaterial = untangibleCloneMaterial;
        if(unitType == UnitType.ENEMY) {
            cloneMaterial = untangibleCloneMaterialEnemy;
        }

        DiceMovement newGhostDice;
        newGhostDice = selectedUnit.GetComponent<DiceFaces>().MakeUntangibleClone(cloneMaterial);
        newGhostDice.MoveToDirecion(Vector3.right, executeEffect: false, grayOut: false);
        selectableMovesDice.Add(newGhostDice);

        newGhostDice = selectedUnit.GetComponent<DiceFaces>().MakeUntangibleClone(cloneMaterial);
        newGhostDice.MoveToDirecion(Vector3.left, executeEffect: false, grayOut: false);
        selectableMovesDice.Add(newGhostDice);

        newGhostDice = selectedUnit.GetComponent<DiceFaces>().MakeUntangibleClone(cloneMaterial);
        newGhostDice.MoveToDirecion(Vector3.up, executeEffect: false, grayOut: false);
        selectableMovesDice.Add(newGhostDice);

        newGhostDice = selectedUnit.GetComponent<DiceFaces>().MakeUntangibleClone(cloneMaterial);
        newGhostDice.MoveToDirecion(Vector3.down, executeEffect: false, grayOut: false);
        selectableMovesDice.Add(newGhostDice);
    }

    public void SelectTile(TileSquare tile) {
        if(IsAnAllyUnitSelected && tile.tileType == TileType.Selectable) {
            Vector3 diff = tile.transform.position - selectedUnit.transform.position;
            //Debug.Log("Diff: " + diff);
            // TODO: Move selected unit to diff
            selectedUnit.MoveToDirecion(diff);
            DeselectCurrentUnit();
        }
        else {
            DeselectCurrentUnit();
        }
    }

    public bool IsAnAllyUnitSelected => selectedUnit?.unitProperties.unitType == UnitType.ALLY;
}