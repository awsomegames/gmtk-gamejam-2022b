using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputListenerOld : MonoBehaviour
{
    [SerializeField] private DiceMovement dieMovement;

    private void OnMove(InputValue inputValue) {
        Vector2 desiredMoveDirection = inputValue.Get<Vector2>();
        //Debug.Log(desiredMoveDirection);
        if(desiredMoveDirection.x > 0) {
            dieMovement.MoveToDirecion(Vector3.right);
        }
        else if(desiredMoveDirection.x < 0) {
            dieMovement.MoveToDirecion(Vector3.left);
        }
        else if(desiredMoveDirection.y > 0) {
            dieMovement.MoveToDirecion(Vector3.up);
        }
        else if(desiredMoveDirection.y < 0) {
            dieMovement.MoveToDirecion(Vector3.down);
        }
    }
}
