using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurnType {
    Player,
    Enemy,
    None
}

public enum TurnPhase {
    Selecting,
    Moving,
    Effect,
    ShowingTurn
}

public class TurnManager : MonoBehaviour
{
    public static TurnManager Instance;
    [SerializeField] private Player player;
    public TurnType initialTurnType;
    public TurnType turnType;
    public TurnPhase turnPhase;
    public UnitType winner = UnitType.NONE;
    public bool alwaysPlayersTurn = false;
    public List<UnitProperties> allUnits;

    public event Action OnTurnSwitchedToPlayer;
    public event Action<TurnType> OnTurnSwitched;

    private SoundManager soundManager;

    #region Hacks
    public void OnPlayerTurn() {
        SwitchTurnTo(TurnType.Player);
    }
    #endregion

    private void Awake()
    {
        Instance = this;
        soundManager = GetComponent<SoundManager>();
    }

    private void Start()
    {
        turnType = initialTurnType;
        if(turnType == TurnType.Player){
            soundManager.PlayClipByName("PlayerTurnBegins");
        }
        else if(turnType == TurnType.Enemy){
            soundManager.PlayClipByName("EnemyTurnBegins");
        }

        turnPhase = TurnPhase.Selecting;

        allUnits = new List<UnitProperties>();
        UnitProperties[] unitsFound = GameObject.FindObjectsOfType<UnitProperties>();
        foreach(UnitProperties unit in unitsFound){
            allUnits.Add(unit);
            unit.diceMovement.StartedMovement += UnitStartedMoving;
            unit.diceMovement.FininshedMovement += UnitHasMoved;
            unit.diceMovement.OnEffectExecuted += UnitEffectStarted;
            unit.diceMovement.OnEffectFinished += UnitEffectHasFinished;
            unit.OnDeath += UnitHasDied;
        }
    }

    private void UnitStartedMoving(UnitProperties unit, Vector3 direction) {
        turnPhase = TurnPhase.Moving;
    }

    private void UnitHasMoved(UnitProperties unit, DiceFaceData diceFaceData, Vector3 direction) {
        // ???
        return;
    }

    private void UnitEffectStarted(UnitProperties unit, Effect effect) {
        turnPhase = TurnPhase.Effect;
        // ???
    }

    private void UnitEffectHasFinished(UnitProperties unit, Effect effect, EffectProperties effectProperties) {
        turnPhase = TurnPhase.Selecting;
        if(unit.unitType == UnitType.ALLY) {
            if(HaveAllUnitsMovedThisTurn(UnitType.ALLY)) {
                SwitchTurnTo(TurnType.Enemy);
            }
        }
        else if(unit.unitType == UnitType.ENEMY) {
            if(HaveAllUnitsMovedThisTurn(UnitType.ENEMY)) {
                SwitchTurnTo(TurnType.Player);
            }
        }
    }

    public void SwitchTurnTo(TurnType newTurnType) {
        if (winner != UnitType.NONE) { return; }

        turnType = newTurnType;
        turnPhase = TurnPhase.Selecting;
        player.DeselectCurrentUnit();
        RefreshAllUnits();

        if(alwaysPlayersTurn) {
            turnType = TurnType.Player;
            OnTurnSwitchedToPlayer?.Invoke();
            return;
        }

        if(newTurnType == TurnType.Player){
            //RefreshAllUnitsOfType(UnitType.Ally);
            OnTurnSwitchedToPlayer?.Invoke();
            soundManager.PlayClipByName("PlayerTurnBegins");
        }
        else if(newTurnType == TurnType.Enemy){
            //RefreshAllUnitsOfType(UnitType.Enemy);
            //EnemyAIController.Instance.StartEnemyTurn();
            soundManager.PlayClipByName("EnemyTurnBegins");
        }

        if(newTurnType != TurnType.None) {
            turnPhase = TurnPhase.ShowingTurn;
            OnTurnSwitched?.Invoke(turnType);
        }
    }

    public void EndShowingTurn() {
        if(winner == UnitType.NONE) {
            if(turnType == TurnType.Enemy) {
                turnPhase = TurnPhase.Selecting;
                EnemyAIController.Instance.StartEnemyTurn();
                soundManager.PlayClipByName("EnemyTurnTheme", true);
            }
            else if(turnType == TurnType.Player) {
                turnPhase = TurnPhase.Selecting;
                soundManager.PlayClipByName("PlayerTurnTheme", true);
            }
        }
        else if(winner == UnitType.ALLY) {
            // TODO: PLAYER WON, next level
            Debug.Log("Player won! Loading next level!");
            SceneManagerScript.Instance.LoadLevelByName(LevelSubmanager.Instance.nextLevelName, 1f);
        }
        else if(winner == UnitType.ENEMY) {
            // TODO: PLAYER LOST, reload level
            Debug.Log("Player Lost! Reloading level!");
            SceneManagerScript.Instance.ReloadLevel(1f);
        }
        
    }

    private bool HaveAllUnitsMovedThisTurn(UnitType unitsType) {
        foreach(UnitProperties unit in allUnits) {
            if(unit.unitType == unitsType && !unit.hasAlreadyMovedThisTurn) {
                return false;
            }
        }
        return true;
    }

    private void RefreshAllUnitsOfType(UnitType unitsType) {
        foreach(UnitProperties unit in allUnits) {
            if(unit.unitType == unitsType) {
                unit.diceMovement.RefreshUnit();
            }
        }
    }

    private void RefreshAllUnits() {
        foreach(UnitProperties unit in allUnits) {
            unit.diceMovement.RefreshUnit();
        }
    }

    private void UnitHasDied(UnitProperties unit){
        allUnits.Remove(unit);
        if(AreAllUnitsDead(unit.unitType)) {
            // TODO: Lose/Win condition
            if(unit.unitType == UnitType.ALLY) {
                // TODO: Lose condition
                winner = UnitType.ENEMY;
                UIManager.Instance.ShowWinner("ENEMY WINS", UnitType.ENEMY);
            }
            else if(unit.unitType == UnitType.ENEMY) {
                // TODO: Win condition
                winner = UnitType.ALLY;
                UIManager.Instance.ShowWinner("PLAYER WINS", UnitType.ALLY);
            }
        }
    }

    private bool AreThereUnitsOfType(UnitType unitType){
        foreach(UnitProperties unit in allUnits) {
            if(unit.unitType == unitType) {
                return true;
            }
        }
        return false;
    }

    private bool AreAllUnitsDead(UnitType unitType) => !AreThereUnitsOfType(unitType);
}
