using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public static CombatManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    public void OnAttackReceived(EffectProperties effectProperties, UnitProperties attacker, GameObject receiver) {
        if(receiver.GetComponent<KillerLimits>() != null) {
            return;
        }
        UnitProperties receiverUnit = receiver.GetComponentInParent<UnitProperties>();
        if(receiverUnit != null) {
            effectProperties.ApplyEffectToUnit(receiverUnit);
            // The attack hit a unit
            
            return;
        }
        ShieldProperties receiverShield = receiver.GetComponent<ShieldProperties>();
        if(receiverShield != null) {
            // The attack hit a shield
            Debug.Log("Shielded!");
            effectProperties.ApplyEffectToShield(receiverShield);
        }
    }

    public void DamageUnit(UnitProperties unit, int damageValue) {
        // Play visual effect
        unit.animator.Play("Hurt");
        Debug.Log("Hurt! " + unit.animator.GetCurrentAnimatorClipInfo(0));
        // Play sound
        // TODO: Do damage
        int originalHealth = unit.currentHealth;
        unit.currentHealth -= damageValue;
        if(unit.currentHealth < 0){
            unit.currentHealth = 0;
        }
        Debug.Log($"HP: {originalHealth} -> {unit.currentHealth}");
        // TODO:  UI Feedback
        // Check if unit should die
        if(unit.currentHealth == 0) {
            unit.Die();
        }
    }

    public void HealUnit(UnitProperties unit, int healValue) {
        // Play visual effect
        unit.animator.Play("Heal");
        // Play sound
        // TODO: Do heal
        unit.currentHealth += healValue;
        if(unit.currentHealth > unit.maxHealth){
            unit.currentHealth = unit.maxHealth;
        }
        // TODO: UI Feedback
    }
}