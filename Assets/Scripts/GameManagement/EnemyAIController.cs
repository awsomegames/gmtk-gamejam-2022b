using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAIController : MonoBehaviour
{
    public static EnemyAIController Instance;
    private List<UnitProperties> allEnemyUnits;
    private List<UnitProperties> allPlayerUnits;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        UnitProperties[] allUnits = GameObject.FindObjectsOfType<UnitProperties>();
        allEnemyUnits = new List<UnitProperties>();
        allPlayerUnits = new List<UnitProperties>();
        foreach(UnitProperties unit in allUnits) {
            if(unit.unitType == UnitType.ENEMY) {
                allEnemyUnits.Add(unit);
                unit.OnDeath += UnitHasDied;
            }
            else if(unit.unitType == UnitType.ALLY) {
                allPlayerUnits.Add(unit);
                unit.OnDeath += UnitHasDied;
            }
        }
    }

    public void StartEnemyTurn() {
        StartCoroutine(EnemiesTurnCoroutine());
    }

    private IEnumerator EnemiesTurnCoroutine() {
        foreach(UnitProperties enemyUnit in allEnemyUnits) {
            currentUnitEffectFinished = false;
            enemyUnit.diceMovement.OnEffectFinished += FinishCurrentUnitEffect;
            MoveEnemyUnit(enemyUnit);
            yield return new WaitForSeconds(0.5f);
            yield return new WaitUntil(CurrentUnitEffectFinished);
        }
        yield return null;
    }

    private void MoveEnemyUnit(UnitProperties enemyUnit) {
        // TODO: Calculate move direction
        UnitProperties closestPlayerUnit = FindClosestPlayerUnit(enemyUnit);
        Vector3 diff = enemyUnit.transform.position - closestPlayerUnit.transform.position;
        Vector3 direction;

        if(Mathf.Abs(diff.x) >= Mathf.Abs(diff.y)) {
            direction = new Vector3(
                -Mathf.Sign(diff.x),
                0,
                0
            );
        }
        else {
            direction = new Vector3(
                0,
                -Mathf.Sign(diff.y),
                0
            );
        }

        direction = GetValidDirection(enemyUnit, direction);
        enemyUnit.diceMovement.MoveToDirecion(direction);
    }

    private Vector3 GetValidDirection(UnitProperties enemyUnit, Vector3 desiredDirection) {

        Vector3[] extraDirections = new Vector3[] {
            new Vector3(desiredDirection.y, desiredDirection.x, 0),
            new Vector3(-desiredDirection.y, -desiredDirection.x, 0),
            -desiredDirection
        };

        RandomizeArray(extraDirections);
        Vector3[] directions = new Vector3[] {
            desiredDirection,
            extraDirections[0],
            extraDirections[1],
            extraDirections[2]
        };

        foreach(Vector3 dir in directions) {
            RaycastHit hit;
            bool didHit = Physics.Raycast(
                enemyUnit.diceMovement.faceDetector.transform.position + dir,
                Vector3.forward,
                out hit,
                50f
            );
            if(didHit) {
                //Debug.Log("Collision: " + hit.collider.name);
                TileSquare tile = hit.collider.GetComponent<TileSquare>();
                if(tile != null) {
                    return dir;
                }
            }   
        }
        return Vector3.zero;
    }

    private void RandomizeArray<T>(T[] array) {
        int l = array.Length;
        for (int i = 0; i < l; i++)
        {
            int r = Random.Range(0, l);
            T aux = array[i];
            array[i] = array[r];
            array[r] = aux;
        }
    }

    private UnitProperties FindClosestPlayerUnit(UnitProperties enemyUnit) {
        float minDistance = Mathf.Infinity;
        UnitProperties playerClosestUnit = null;
        foreach(UnitProperties playerUnit in allPlayerUnits) {
            float distance = Vector3.Distance(enemyUnit.transform.position, playerUnit.transform.position);
            if(distance < minDistance) {
                minDistance = distance;
                playerClosestUnit = playerUnit;
            }
        }
        return playerClosestUnit;
    }

    private bool currentUnitEffectFinished = false;
    private bool CurrentUnitEffectFinished() => currentUnitEffectFinished;

    private void FinishCurrentUnitEffect(UnitProperties unitProperties, Effect effect, EffectProperties effectProperties) {
        currentUnitEffectFinished = true;
    }

    private void UnitHasDied(UnitProperties unit) {
        allEnemyUnits.Remove(unit);
        allPlayerUnits.Remove(unit);
    }
}
