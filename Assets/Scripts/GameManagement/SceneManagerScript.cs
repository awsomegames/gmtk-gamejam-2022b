using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public static SceneManagerScript Instance;
    [Tooltip("Levels in their proper order.")]
    public List<string> levelNames;
    private int currentLevelIndex = 0;

    private void Awake()
    {
        if(Instance == null) {
            Instance = this;
            DontDestroyOnLoad(Instance/*.transform.parent*/);
        }
        else {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        currentLevelIndex = IndexOflLevel(SceneManager.GetActiveScene().name);
    }

    private int IndexOflLevel(string sceneName) {
        int i = 0;
        foreach(string levelName in levelNames) {
            if (levelName == sceneName) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private void ReloadScene() {
        string sceneToLoad = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneToLoad);
    }

    public IEnumerator ReloadSceneCoroutine(float delay) {
        yield return new WaitForSeconds(delay);
        ReloadScene();
        yield return null;
    }

    public void ReloadLevel(float delay) {
        StartCoroutine(LoadSceneByNameCoroutine("", delay));
    }

    public void LoadNextLevel(float delay)
    {
        int nextLevelIndex = currentLevelIndex + 1;
        string nextLevelName = "";
        try {
            nextLevelName = levelNames[nextLevelIndex];
        }
        catch(System.ArgumentOutOfRangeException ex) {
            Debug.LogWarning("No more levels in list!\n" + ex.Message);
            StartCoroutine(LoadSceneByNameCoroutine("", delay));
            return;
        }

        StartCoroutine(LoadSceneByNameCoroutine(nextLevelName, delay));
        currentLevelIndex++;
    }

    public void LoadLevelByName(string levelName, float delay)
    {
        StartCoroutine(LoadSceneByNameCoroutine(levelName, delay));
    }

    private IEnumerator LoadSceneByNameCoroutine(string nextLevelName, float delay) {
        yield return new WaitForSeconds(delay);
        LoadSceneByName(nextLevelName);
        yield return null;
    }

    public void LoadSceneByName(string nextLevelName) {
        string sceneToLoad = nextLevelName;
        if(sceneToLoad == "") {
            sceneToLoad = SceneManager.GetActiveScene().name;
        }
        SceneManager.LoadScene(sceneToLoad);
    }

}
