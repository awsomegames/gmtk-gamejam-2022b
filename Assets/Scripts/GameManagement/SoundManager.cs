using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioInfo {
    public string name;
    public AudioClip audioClip;
    public float volume = 1f;
    public float pitch = 1f;
}

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    private AudioSource audioSource;
    public List<AudioInfo> audiosInfo;

    private void Awake(){
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayCurrentClip(bool loop = false){
        Debug.Log("Playing clip!");
        audioSource.loop = loop;
        audioSource.Play();
    }

    public void SetPitch(float pitch) {
        audioSource.pitch = pitch;
    }

    public AudioInfo FindAudioInfoByName(string clipName) {
        foreach(AudioInfo audioInfo in audiosInfo) {
            if(audioInfo.name == clipName) {
                return audioInfo;
            }
        }
        return null;
    }

    public void SetCurrentAudioClip(string clipName) {
        AudioInfo audioInfo = FindAudioInfoByName(clipName);
        if(audioInfo == null) {
            throw new System.Exception(
                $"Not such an AudioInfo with the name \"{clipName}\"" +
                $"in the object {transform.name}."
            );
        }
        audioSource.clip = audioInfo.audioClip;
        audioSource.volume = audioInfo.volume;
        audioSource.pitch = audioInfo.pitch;
    }

    public void PlayClipByName(string clipName, bool loop = false) {
        SetCurrentAudioClip(clipName);
        PlayCurrentClip(loop);
    }
}
