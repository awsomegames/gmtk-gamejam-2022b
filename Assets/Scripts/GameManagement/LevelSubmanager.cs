using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSubmanager : MonoBehaviour
{
    public static LevelSubmanager Instance;
    public string nextLevelName;

    private void Awake()
    {
        Instance = this;
    }

    public void GoToTitleScreen() {
        SceneManagerScript.Instance.LoadSceneByName("TitleScreen");
    }

    public void LoadNextLevel() {
        SceneManagerScript.Instance.LoadSceneByName(nextLevelName);
    }

    public void ReloadLevel() {
        SceneManagerScript.Instance.ReloadLevel(0);
    }

    public void UnpauseGame() {
        GameManager.Instance.UnpauseGame();
    }

}
