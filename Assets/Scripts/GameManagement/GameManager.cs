using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool isGamePaused = false;

    public static GameManager Instance;

    private void Awake()
    {
        if(Instance == null) {
            Instance = this;
            DontDestroyOnLoad(Instance);
        }
        else {
            Destroy(gameObject);
        }
        SceneManager.sceneLoaded += OnSceneLoad;
    }

    public void PauseGame() {
        Debug.Log("Pausing game!");
        isGamePaused = true;
        // TODO: Show pause menu
        UIManager.Instance.ShowPauseMenu();
    }

    public void UnpauseGame() {
        Debug.Log("Unpausing game!");
        isGamePaused = false;
        // TODO: Hide pause menu
        UIManager.Instance.HidePauseMenu();
    }

    public void TogglePause(){
        if(isGamePaused) {
            UnpauseGame();
        }
        else {
            PauseGame();
        }
    }

    public void OnSceneLoad(Scene scene, LoadSceneMode mode) {
        GameManager.Instance.UnpauseGame();
    }
}
