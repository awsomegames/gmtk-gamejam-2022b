using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CollisionCallback : MonoBehaviour{

    public event Action<Collider> OnTriggerEnterEvent;
    public event Action<Collision> OnCollisionEnterEvent;

    private void OnTriggerEnter(Collider other)
    {
        OnTriggerEnterEvent?.Invoke(other);
    }

    private void OnCollisionEnter(Collision other)
    {
        OnCollisionEnterEvent?.Invoke(other);
    }
}