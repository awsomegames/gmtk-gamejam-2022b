using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationCallback : MonoBehaviour
{
    public event Action<string> OnAnimationEvent;
    public void AnimationMessage(string message) {
        OnAnimationEvent?.Invoke(message);
    }
}